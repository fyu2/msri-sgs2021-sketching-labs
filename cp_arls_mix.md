# CP-ARLS with Mixing for Tensor Labs for MSRI Summer Graduate School on Sketching, 2021

Tamara G. Kolda, tammy.kolda@mathsci.ai 

See [README.md](README.md) for full acknowledgements and other modules.

The purpose of this module is to give the student experience with randomized least squares and using the CP-ARLS code.

## Johnson Lindenstrauss Transforms

Prove to yourself that JLTs work!

- [ ] Generate a set of $`p`$ vectors, $`x_i`$ of length $`n`$ (e.g., $`p = 1000, n = 5000`$)
- [ ] For a given value of $`m`$, create a JLT $`\Phi`$ with normal distrbuted entries (be sure to get the scaling right)
- [ ] Compute the mean deviation, i.e.,
```math
\frac{ | \| x_i \| - \| \Phi x_i \| | } { \|x_i\| }
```
- [ ] Compute this for $`m = 50, 100, 200, 400, 800`$ and plot the mean deviations

## Do the same as above, but use a *fast* JLT (FJLT)

_Be careful because the result of the FFT is complex!_

## Solve large least squares problem using FJLT

- [ ] Create an overdetermined least squares problem $`\|Ax-b\|`$ of size $`n \times p`$ with $`n=50,000`$ and $`p=100`$.
- [ ] Solve it the usual way using the MATLAB backslash (`help slash` for more information)
- [ ] Compress it using FJLT. This results in a complex system. Make it real as follows
```math
\begin{bmatrix} \text{real}(\hat A) \\ \text{imag}(\hat A) \end{bmatrix}
x =
\begin{bmatrix} \text{real}(\hat b) \\ \text{imag}(\hat b) \end{bmatrix}
```
- [ ] Compare the time for the two methods
- [ ] Compare the relative error w.r.t. the original system, i.e., $`\|Ax-b\|`$

## Bonus

- [ ] Create large artificial problems and use them to compare CP-ALS and CP-ARLS (with and without mixing)
- [ ] Create an artificial problem where CP-ARLS _without_ mixing fails to get the solution. What does such a problem look like?

