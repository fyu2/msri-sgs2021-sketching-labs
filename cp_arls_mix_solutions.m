%% JLTs
clear;
n = 5000;
p = 1000;
X = rand(n,p);

for i = 1:5
    m = 25*2^i;
    Phi = 1/sqrt(m) * randn(m,n);
    
    Xhat = Phi*X;
    
    n1 = sqrt(sum(X.^2));
    n2 = sqrt(sum(Xhat.^2));
    diffs = abs(n1-n2)./n1;
    x(i) = m;
    y(i) = mean(diffs);
end

figure(1); clf;
plot(x,y)

%% FJLTs

clear;
n = 5000;
p = 1000;
X = rand(n,p);

for i = 1:5
    m = 25*2^i;
    
    % generate random sign-flipping operation
    sgnflips = sign(rand(n,1)-0.5);
    XX = bsxfun(@times,X,sgnflips);
    
    % mixing
    Y = fft(XX);
    
    % select which rows to sample
    sidx = randsample(n,m);
    
    % compress
    Xhat = (1/sqrt(m)).*Y(sidx,:);
       
    n1 = sqrt(sum(X.^2));
    n2 = sqrt(sum(Xhat.*conj(Xhat)));
    diffs = abs(n1-n2)./n1;
    x(i) = m;
    y(i) = mean(diffs);
end

figure(1); clf;
plot(x,y)

%% Solve large least squares use FJLT
clear;
n = 50000;
p = 100;
m = 200;
A = rand(n,p);
b = rand(n,1);

% Full solve
tic
x = A \ b;
toc
r = norm(A*x-b);

% FJLT solve
tic
sgnflips = sign(rand(n,1)-0.5);
AA = bsxfun(@times,A,sgnflips);
bb = bsxfun(@times,b,sgnflips);
% mixing
AAA = fft(AA);
bbb = fft(bb);

% select which rows to sample
sidx = randsample(n,m);

% compress
Ahat = (1/sqrt(m)).*AAA(sidx,:);
bhat = (1/sqrt(m)).*bbb(sidx,:);

% Keep it real
Afoo = [real(Ahat); imag(Ahat)];
bfoo = [real(bhat); imag(bhat)];
xfoo = Afoo \ bfoo;

toc

% residual wrt original system
rfoo = A * xfoo - b;


fprintf('FJLT rel residual diff: ');
abs(norm(r)-norm(rfoo))/norm(r)




