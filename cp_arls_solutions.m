%% Create a 400 x 400 x 400 problem with high congruence
d = 3;
sz = [400 400 400];
r = 5;
c = 0.8;
A = cell(3,1);
for k = 1:d
    A{k} = matrandcong(sz(k),r,c);
end
Mtrue = ktensor(A);

%% Add noise
eta = 0.01;
R = tensor(@randn,size(Mtrue));
Xtrue = full(Mtrue);
X = Xtrue + eta * (norm(Xtrue) / norm(R)) * R;

%% Run CP-ALS (do this a few times)
tic 
M = cp_als(X,r);
toc
1-norm(X-M)/norm(X)

%% Run CP-ARLS (do this a few times)
tic 
M = cp_arls(X,r,'mix',false,'epoch',5,'printitn',1);
toc
1-norm(X-M)/norm(X)

%% Gas data
clear 
load gas/gas3

%% CP-ARLS for Gas data
tic
M = cp_als(X,5);
toc
viz(M, 'Figure',3, gasvizopts{:})

%%
tic
Mr = cp_arls(X,5,'mix',false,'epoch',5,'printitn',1);
toc
1-norm(X-full(Mr))/norm(X)
viz(Mr, 'Figure',4, gasvizopts{:})